# eyeo's Monitoring Plugins

This repository contains custom check plugins for monitoring software like
[Nagios](https://nagios.org/) and [Icinga](https://icinga.com/).
